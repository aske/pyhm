from itertools import *

class NameGen(object):
  def __init__(self):
    self.vars = {}

  def __call__(self, name):
    if name not in self.vars:
      self.vars[name] = 0
    else:
      self.vars[name] += 1
    return "{}_{}".format(name, self.vars[name])