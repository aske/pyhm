from sumtypes import *
from pyrsistent import pmap

from names import *
from expr import *
from type import *

def apply_vars(types0, vars):
  def apply_one(type):
    @match(Type)
    class apply_match(object):
      def Builtin(_):
        return type

      def Var(v):
        if v in vars:
          return vars[v]
        else:
          return type

      def Arr(t1, t2):
        return Type.Arr(apply_one(t1), apply_one(t2))
    return apply_match(type)

  def apply_list(types):
    if isinstance(types, Type):
      return apply_one(types)
    else:
      return type(types)(map(apply_list, types))

  return apply_list(types0)

def infer_constraints(expr0):
  gen = NameGen()
  constraints = []

  def instantiate(scheme):
    vmap = {k : Type.Var(gen(k)) for k in scheme.vars}
    return apply_vars(scheme.type, vmap)

  def infer_expr(expr, env):
    def generalize(type):
      @match(Type)
      class generalize_match(object):
        def Builtin(_):
          return free_scheme(type)

        def Var(v):
          if v in env:
            return free_scheme(type)
          else:
            return scheme({v}, type)

        def Arr(t1, t2):
          s1 = generalize(t1)
          s2 = generalize(t2)
          return scheme(s1.vars | s2.vars, Type.Arr(t1, t2))
      return generalize_match(type)
    
    @match(Expr)
    class infer_match(object):
      def Int(val):
        return int_type

      def Bool(val):
        return bool_type

      def Builtin(op):
        return instantiate(builtin_type(op))

      def Var(name):
        return instantiate(env[name])

      def Lam(name, expr1):
        tn = Type.Var(name)
        t1 = infer_expr(expr1, env.set(name, free_scheme(tn)))
        return Type.Arr(tn, t1)

      def App(expr1, app):
        t1 = infer_expr(expr1, env)
        t2 = infer_expr(app, env)
        tn = Type.Var(gen("#app"))
        constraints.append((t1, Type.Arr(t2, tn)))
        return tn

      def Let(name, bind, expr):
        tb = infer_expr(bind, env)
        sc = generalize(tb)
        return infer_expr(expr, env.set(name, sc))
    return infer_match(expr)

  t = infer_expr(expr0, pmap())
  return t, constraints

def compose_subst(s1, s2):
  s = {k : apply_vars(v, s1) for k, v in s2.items()}
  s.update(s1)
  return s

@match(Type)
class ftv(object):
  def Builtin(_):
    return set()

  def Var(v):
    return {v}

  def Arr(t1, t2):
    return ftv(t1) | ftv(t2)

def bind_var(name, type):
  if isinstance(type, Type.Var) and type.name == name:
    return {}
  elif name in ftv(type):
    raise ValueError("Infinite type: {} ~ {}", name, type)
  else:
    return {name: type}

def unify(t1, t2):
  if t1 == t2:
    return {}
  elif isinstance(t1, Type.Var):
    return bind_var(t1.name, t2)
  elif isinstance(t2, Type.Var):
    return bind_var(t2.name, t1)
  elif isinstance(t1, Type.Arr) and isinstance(t2, Type.Arr):
    su1 = unify(t1.t1, t2.t1)
    su2 = unify(apply_vars(t1.t2, su1), apply_vars(t2.t2, su1))
    return compose_subst(su2, su1)
  else:
    raise ValueError("Unification fail: {} !~ {}".format(t1, t2))

def solve_constraints(cs):
  subst = {}

  while len(cs) != 0:
    su1 = unify(cs[0][0], cs[0][1])
    cs = apply_vars(cs[1:], su1)
    subst = compose_subst(su1, subst)

  return subst

def infer(expr):
  t, cs = infer_constraints(expr)
  st = solve_constraints(cs)
  return apply_vars(t, st)