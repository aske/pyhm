from functools import *
from pyparsing import *
from pyrsistent import pmap

from expr import *

expr = Forward()

reserved_names = {"let", "in", "if", "true", "false"}

name = Regex("[a-z_][a-z0-9_]*").addCondition(lambda toks: toks[0] not in reserved_names)

eint = Word(nums).addParseAction(lambda toks: [Expr.Int(int(toks[0]))])
ebool = Literal("true").addParseAction(lambda: [Expr.Bool(True)]) | \
        Literal("false").addParseAction(lambda: [Expr.Bool(False)])

evar = name.copy().addParseAction(lambda toks: [Expr.Var(toks[0])])

ebuiltin = Literal("if").addParseAction(lambda: [Expr.Builtin(Builtin.If())])

atom = eint | ebool | evar | ebuiltin | Literal("(").suppress() + expr + Literal(")").suppress()

eapp = (atom + ZeroOrMore(White().suppress() + atom)).addParseAction(lambda toks: [reduce(Expr.App, toks)])

bplusop = Literal("+").addParseAction(lambda: [Expr.Builtin(Builtin.Plus())])
bminusop = Literal("-").addParseAction(lambda: [Expr.Builtin(Builtin.Minus())])
bmulop = Literal("*").addParseAction(lambda: [Expr.Builtin(Builtin.Mul())])
beqlop = Literal("==").addParseAction(lambda: [Expr.Builtin(Builtin.Eql())])

def reduce_op(toks):
  while len(toks) != 1:
    toks = [ Expr.App(Expr.App(toks[1], toks[0]), toks[2]) ] + toks[3:]
  return [toks[0]]

bmul = (eapp + ZeroOrMore(bmulop + eapp)).addParseAction(reduce_op)
bsum = (bmul + ZeroOrMore((bplusop | bminusop) + bmul)).addParseAction(reduce_op)
beql = (bsum + ZeroOrMore(beqlop + bsum)).addParseAction(reduce_op)

elet = (Literal("let").suppress() + name + Literal("=").suppress() + expr + \
        Literal("in").suppress() + expr
       ).addParseAction(lambda toks: [Expr.Let(toks[0], toks[1], toks[2])]) | beql

elam = (Literal("\\").suppress() + name + Literal("->").suppress() + expr).addParseAction(lambda toks: [Expr.Lam(toks[0], toks[1])]) | elet

expr <<= elam
