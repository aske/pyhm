#!/usr/bin/env python3

import unittest

import parser
import rename
import inference

def parse_str(input):
    return parser.expr.parseString(input, parseAll=True)[0]

def inf_routine(expr):
    re = rename.rename(expr)
    t, cs = inference.infer_constraints(re)
    return inference.infer(re)

class TestInference(unittest.TestCase):
    def setUp(self):
        pass

    def test_single_int(self):
        e = parse_str(r"123")
        self.assertEqual(str(inf_routine(e)), "Int")

    def test_single_bool(self):
        r1 = inf_routine(parse_str(r"false"))
        r2 = inf_routine(parse_str(r"true"))
        self.assertEqual(r1, r2)
        self.assertEqual(str(r1), "Bool")

    def test_int_op(self):
        r = inf_routine(parse_str(r"1 + 2"))
        self.assertEqual(str(r), "Int")

    def test_int_comp(self):
        r = inf_routine(parse_str(r"1 == 2"))
        self.assertEqual(str(r), "Bool")

    def test_lam(self):
        r = inf_routine(parse_str(r"\x -> x"))
        self.assertEqual(str(r), "(x_0 -> x_0)")

    def test_simple(self):
       r = inf_routine(parse_str(r"\x -> let a = \x -> x in 1 + 2 - a x"))
       self.assertEqual(str(r), "(Int -> Int)")

    def test_gen(self):
        r = inf_routine(parse_str(r"let a = \x -> x in if (a (1 == 1)) 2 (a 3)"))
        self.assertEqual(str(r), "Int")

    def test_var_error(self):
        with self.assertRaisesRegex(ValueError, ".*Variable.*undefined.*") as cm:
            inf_routine(parse_str(r"\x -> y"))

    def test_occurs_error(self):
        with self.assertRaisesRegex(ValueError, ".*Infinite type.*") as cm:
            inf_routine(parse_str(r"\x -> x x"))

    def test_unif_error(self):
        with self.assertRaisesRegex(ValueError, ".*Unification.*fail.*") as cm:
            inf_routine(parse_str(r"let f = \x -> x in let a = true in f (a + false)"))
            
if __name__ == '__main__':
    unittest.main()
