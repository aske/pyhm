from pyrsistent import pmap

from names import *
from expr import *

def rename(expr0):
  gen = NameGen()

  def rename_int(expr, env):
    @match(Expr)
    class rename_match(object):
      def Int(val):
        return expr

      def Bool(val):
        return expr

      def Builtin(op):
        return expr

      def Var(name):
        if name not in env:
          raise ValueError("Variable {} is undefined".format(name))
        return Expr.Var(env[name])

      def Lam(name, expr1):
        n = gen(name)
        return Expr.Lam(n, rename_int(expr1, env.set(name, n)))

      def App(expr, app):
        return Expr.App(rename_int(expr, env), rename_int(app, env))

      def Let(name, bind, expr):
        n = gen(name)
        return Expr.Let(n, rename_int(bind, env), rename_int(expr, env.set(name, n)))
    return rename_match(expr)

  return rename_int(expr0, pmap())
