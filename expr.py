from functools import *
from sumtypes import *
from pyrsistent import PMap

from utils import *

@sumtype
class Builtin(object):
  Plus = constructor()
  Minus = constructor()
  Mul = constructor()
  If = constructor()
  Eql = constructor()

@match(Builtin)
class str_builtin(object):
  def Plus():
    return "+"

  def Minus():
    return "-"

  def Mul():
    return "*"

  def If():
    return "if"

  def Eql():
    return "=="

Builtin.__str__ = str_builtin

# Hack to allow recursive referencing
class Expr(object):
  pass

Expr.Int = constructor(val=typed(int))
Expr.Bool = constructor(val=typed(bool))
Expr.Builtin = constructor(op=typed(Builtin))
Expr.Var = constructor(name=typed(str))
Expr.Lam = constructor(name=typed(str), expr=typed(Expr))
Expr.App = constructor(expr=typed(Expr), app=typed(Expr))
Expr.Let = constructor(name=typed(str), bind=typed(Expr), expr=typed(Expr))

Expr = sumtype(Expr)

@match(Expr)
class str_expr(object):
  def Int(val):
    return str(val)

  def Bool(val):
    return str(val)

  def Builtin(op):
    return str(op)

  def Var(name):
    return str(name)

  def Lam(name, expr):
    return "(\\{} -> {})".format(name, expr)

  def App(expr, app):
    return "({} {})".format(expr, app)

  def Let(name, bind, expr):
    return "(let {} = {} in {})".format(name, bind, expr)

Expr.__str__ = str_expr
