import attr

def typed(typ):
  return attr.ib(validator=attr.validators.instance_of(typ))