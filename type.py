from sumtypes import *
from pyrsistent import PSet, PRecord, field, pset

from utils import *
from expr import *

@sumtype
class BuiltinType(object):
  Int = constructor()
  Bool = constructor()

@match(BuiltinType)
class str_builtin_type(object):
  def Int():
    return "Int"

  def Bool():
    return "Bool"

BuiltinType.__str__ = str_builtin_type

class Type(object):
  pass

Type.Builtin = constructor(t=typed(BuiltinType))
Type.Var = constructor(name=typed(str))
Type.Arr = constructor(t1=typed(Type), t2=typed(Type))

Type = sumtype(Type)

int_type = Type.Builtin(BuiltinType.Int())
bool_type = Type.Builtin(BuiltinType.Bool())

@match(Type)
class str_type(object):
  def Builtin(t):
    return str(t)

  def Var(name):
    return str(name)

  def Arr(t1, t2):
    return "({} -> {})".format(t1, t2)

Type.__str__ = str_type

class Scheme(PRecord):
  vars = field(type=PSet)
  type = field(type=Type)

def scheme(vars, type):
  return Scheme(vars=pset(vars), type=type)

def free_scheme(type):
  return scheme({}, type)

@match(Builtin)
class builtin_type(object):
  def Plus():
    return free_scheme(Type.Arr(int_type, Type.Arr(int_type, int_type)))

  def Minus():
    return free_scheme(Type.Arr(int_type, Type.Arr(int_type, int_type)))

  def Mul():
    return free_scheme(Type.Arr(int_type, Type.Arr(int_type, int_type)))

  def If():
    varA = Type.Var("a")
    return scheme({"a"}, Type.Arr(bool_type, Type.Arr(varA, Type.Arr(varA, varA))))

  def Eql():
    return free_scheme(Type.Arr(int_type, Type.Arr(int_type, bool_type)))
