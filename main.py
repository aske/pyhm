#!/usr/bin/env python3

import sys

import parser
import rename
import inference

def main():
  if len(sys.argv) != 2:
    print("Usage: {} expr".format(sys.argv[0]))
    exit(1)

  try:
    es = parser.expr.parseString(sys.argv[1], parseAll=True)
    if len(es) != 1:
      print("Ambiguous input: {}".format(es))
      exit(1)
    e = es[0]

    print(e)
    re = rename.rename(e)
    print(re)
    t, cs = inference.infer_constraints(re)
    print("Expression type: {}".format(t))
    print()
    for c1, c2 in cs:
      print("{} ~ {}".format(c1, c2))
      print()
      print("Result: {}".format(inference.infer(re)))
  except ValueError as err:
    print("Error occured: ")
    print(err)
  except parser.ParseException:
    print("Error: Invalid syntax")

if __name__ == "__main__":
  main()
